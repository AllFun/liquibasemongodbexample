Liquibase mongoDB example based on article:
https://docs.liquibase.com/workflows/database-setup-tutorials/mongodb.html

Additionally required library:
https://mvnrepository.com/artifact/org.mongodb/bson/4.3.3
or direct link:
https://repo1.maven.org/maven2/org/mongodb/bson/4.3.3/bson-4.3.3.jar
