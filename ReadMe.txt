Test project for test locally Mongo with liquibase-mongo-db

All necessary scripts are in clear-liquibase-example directory.


Root path commands:
For starting test in-mem mongo-db server execute in command line:
mvn spring-boot:run

For executing migration:
    * clear-liquibase-example\run-migration.bat
    * mvn liquibase:update

For executing clean:
    * clear-liquibase-example\run-dropAll.bat
    * mvn liquibase:dropAll