package com.edu.liquibase.mongodb.config;

import com.mongodb.client.MongoClients;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.ImmutableMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.config.Storage;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
public class StartMongoServer {

    private static final String CONNECTION_STRING = "mongodb://%s:%d";

    @Value("${mongo.db.connection.host}")
    private String dbHost;

    @Value("${mongo.db.connection.port}")
    private Integer dbPort;

    @Value("${mongo.db.name}")
    private String dbName;

    @Value("${mongo.db.storage}")
    private String dbStorage;

    @Bean
    @SneakyThrows
    public ImmutableMongodConfig immutableMongodConfig() {
        return MongodConfig
                .builder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(dbHost, dbPort, Network.localhostIsIPv6()))
                .replication(new Storage(dbStorage, null, 0))
                .build();

    }

    @Bean
    public MongodStarter mongodStarter() {
        return MongodStarter.getDefaultInstance();
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    @SneakyThrows
    public MongodExecutable mongodExecutable(final MongodStarter starter, final ImmutableMongodConfig mongodConfig) {
        final MongodExecutable mongodExecutable = starter.prepare(mongodConfig);
        return mongodExecutable;
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(MongoClients.create(format(CONNECTION_STRING, dbHost, dbPort)), dbName);
    }
}
