package com.edu.liquibase.mongodb;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication(exclude = {EmbeddedMongoAutoConfiguration.class})
public class MongoDbInMem {
    public static void main(final String[] args) {
        run(MongoDbInMem.class, args);
    }
}
